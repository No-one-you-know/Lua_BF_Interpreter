local stack = {}

-- Metatable for fake and basic inheritance :P
local stack_mt = {}

stack_mt.__index = function(self, key)
	return self.__baseclass[ key ]
end

stack_mt.__tostring = function(self)
	local len = self:length()
	return string.format("Stack: %d element%s", len, (len ~= 1 and "s" or "") )
end

-- Create a new stack instance. If 'source_array' is passed then the new
-- stack instance will contain all the elements in that array, being the
-- the first one at the bottom and the last one at the top.
stack.new = function( source_array )
	local new_stack = { __baseclass = stack }
	local status = true

	if source_array then 
		if type(source_array) == "table" then
			for i = 1, #source_array do
				new_stack[i] = source_array[i]
			end
		else
			
		end
	end

	setmetatable( new_stack, stack_mt )
	return new_stack
end

-- Return the amount of elements currently inside a stack instance
stack.length = function( self )
	if not self or type(self) ~= "table" then
		return false
	end

	return #self
end
stack.len = stack.length

-- Add another element to the top (the last element seen as an array) of the
-- stack.
stack.push = function( self, new_element )
	if new_element == nil then
		return false
	end
	
	self[ #self + 1 ] = new_element
	return true
end

-- Returns the element on the top of the stack (or one with an offset)
-- without popping it out of the stack itself.
-- 'offset' must be a positive integer so the actual element being returned
-- would be located at (top_element_position - offset).
stack.peek = function( self, offset )
	if not offset then
		offset = 0
	elseif type(offset) ~= "number" then
		return nil, false
	end

	return self[ #self - offset  ]
end

-- Returns the element atop the stack instance and removes it from the stack
-- as well. Returns nil if the stack is empty.
stack.pop = function( self )
	local element_to_return = self[ #self ]
	
	self[ #self ] = nil

	return element_to_return
end

-- Removes all elements from a stack instance.
stack.clear = function( self )
	if not self or type(self) ~= "table" then
		return false
	end

	for i = 1, #self do
		self[i] = nil
	end

	return true
end

return stack.new()
