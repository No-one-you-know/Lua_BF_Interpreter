#! /bin/env lua

local stack = require 'stack'

local args = {...}
local source, data = {}, {}
local source_pointer, data_pointer = 1, 1
local opcodes, config = {}, {}
local jump_table = {}

local nop = function() end

local opcodes_mt = { 
	__index = function(self, key)
		return nop
	end
}

local data_mt = {
	__index = function(self, key)
		return 0
	end
}

setmetatable( opcodes, opcodes_mt )
setmetatable( data, data_mt )

-- Set configuration variables
do
	-- Maximum value for any cell to store
	config.max_cell_limit = 255 + 1
	
	-- Amount of byte cells for data array
	config.max_cell_amount = 30000 + 1
	
	-- 0 = ASCII, 1 = Numbers only
	config.output_type = 0
	
end

-- Get the source code, by filename or by stdin.
do
	local raw_source_code, raw_source_code_file

	if not args[1] then
		raw_source_code = io.stdin:read("*a")
		io.stdin:flush()
	else
		raw_source_code_file = io.open(args[1], "r")
		if not raw_source_code_file then
			io.stderr:write("Error: Failed trying to open file '"..args[1].."'.\n")
			os.exit(0)
		end
		raw_source_code = raw_source_code_file:read("*a")
		raw_source_code_file:close()
	end

	for i = 1, #raw_source_code do
		source[i] = raw_source_code:sub(i,i)
	end
end

-- Store jump operators ('[' & ']') positions for quick lookup purposes.
do
	local jump_stack = stack.new()
	local tmp

	for i = 1, #source do
		if source[i] == "[" then
			jump_stack:push(i)
		elseif source[i] == "]" then

			if jump_stack:len() <= 0 then
				io.stderr:write( string.format( 
					"Error: Missing opening '[' for the matching ']' at %d byte.\n", i
				))
				os.exit(0)
			end
			
			tmp = jump_stack:pop()
			jump_table[ i ] = tmp
			jump_table[ tmp ] = i
		end
	end

	if jump_stack:len() > 0 then
		io.stderr:write( string.format( 
			"Error: Missing closing ']' for the matching '[' at %d byte.\n", 
			jump_stack:peek()
		))
		os.exit(0)
	end

end

opcodes['>'] = function()
	data_pointer =
		(data_pointer + config.max_cell_amount + 1 ) % config.max_cell_amount
end

opcodes['<'] = function()
	data_pointer =
		(data_pointer + config.max_cell_amount - 1 ) % config.max_cell_amount
end

opcodes['+'] = function()
	data[data_pointer] =
		(data[data_pointer] + config.max_cell_limit + 1 ) % config.max_cell_limit
end

opcodes['-'] = function()
	data[data_pointer] =
		(data[data_pointer] + config.max_cell_limit - 1 ) % config.max_cell_limit
end

if config.output_type == 0 then
	opcodes['.'] = function()
		io.stdout:write( string.char( data[data_pointer] ) )
	end
elseif config.output_type == 1 then
	opcodes['.'] = function()
		io.stdout:write( tostring( data[data_pointer] ) )
	end
end

opcodes[','] = function()
	data[data_pointer] = string.byte( io.stdin:read(1) or "" )
end

opcodes['['] = function()
	if data[data_pointer] == 0 then
		source_pointer = jump_table[source_pointer]
	end
end

opcodes[']'] = function()
	if data[data_pointer] ~= 0 then
		source_pointer = jump_table[source_pointer]
	end
end

while true do
	if not source[source_pointer] then
		os.exit(0)
	end
	
	opcodes[ source[source_pointer] ]()
	source_pointer = source_pointer + 1
end
